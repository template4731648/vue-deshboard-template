import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import Pages from "vite-plugin-pages"
import Unfonts from "unplugin-fonts/vite"
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import Layouts from "vite-plugin-vue-layouts"
import Vuetify from "vite-plugin-vuetify"

export default defineConfig(({ mode }) => ({
  ssr: {
    noExternal: mode === "development" ? ["vue-router"] : [],
  },
  plugins: [
    Unfonts({
      google: {
        families: [
          {
            name: "Kanit",
            styles: "wght@100;300;400;500;700;900",
          },
        ],
      },
    }),
    AutoImport({
      imports: ["vue", "vue-router"],
      dts: true,
      eslintrc: {
        enabled: true,
      },
      vueTemplate: true,
    }),
    Components(),
    Pages({
      dirs: "src/pages",
      exclude: ["**/components/*.vue"],
      extendRoute(route, parent) {
        if (route.path === "/")
          return {
            ...route,
            // redirect: "login",
          }
      },
      importMode: "async",
    }),
    Layouts({
      layoutsDirs: "src/layouts",
      defaultLayout: "deshboard",
    }),
    vue(),
    Vuetify({
      autoImport: true,
      styles: {
        configFile: "src/styles/settings.sass",
      },
    }),
  ],
}))
